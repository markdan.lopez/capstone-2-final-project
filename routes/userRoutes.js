const express = require('express')
const router = express.Router()
const UserController = require('../controllers/userController')
const auth = require('../auth')
const User = require('../models/user')


// Register a user or admin
router.post('/register', (req,res) => {
    UserController.register(req.body).then(result => res.send(result))
})

// Log in a user
router.post('/login', (req, res) => {
    UserController.login(req.body).then(result => {
        res.send(result)
    })
})


// Get all the users details
router.get('/getallusers', auth.verify, auth.checkIsAdmin,(req,res) => {
    UserController.getUsers().then(result => res.send(result))
})


//set user as admin (admin only)
router.put('/setasadmin/:id', auth.verify, auth.checkIsAdmin, (req,res) => {
    UserController.setAdmin(req.params.id).then(result => res.send(result))
})


// Create an order for non-admin
router.post('/createorder', auth.verify, auth.checkNonAdmin, (req,res) => {
    const data = {
        userId: auth.decode(req.headers.authorization).id,
        productId: req.body.productId,
        quantity: req.body.quantity
    }
    UserController.addOrder(data).then(result => res.send(result))
})


// retrive authenticated user's orders
router.get('/myorders',auth.verify, auth.checkNonAdmin, (req,res) => {
    const data = {
        userId: auth.decode(req.headers.authorization).id
    }
    UserController.getOrders(data).then(result => res.send(result))
})


// retrieve all orders (admin only)
router.get('/allorders', auth.verify, auth.checkIsAdmin, (req,res) => {
    UserController.getAllOrders().then(result => res.send(result))
})


module.exports = router;