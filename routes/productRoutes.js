const express = require('express')
const router = express.Router()
const ProductController = require('../controllers/productController')
const auth = require('../auth')
const Product = require('../models/product')


// Create a product (admin only)
router.post('/createproduct',auth.verify, auth.checkIsAdmin, (req,res) => {
    ProductController.addProduct(req.body).then(result => res.send(result))
})

//get all products
router.get('/',auth.verify, (req,res) => {
    ProductController.getActiveProducts().then(result => res.send(result))
})

//get a specific product
router.get('/:id', auth.verify, (req,res) => {
    ProductController.getProduct(req.params.id).then(result => res.send(result))
})

//update product (admin only)
router.put('/:id/update', auth.verify, auth.checkIsAdmin, (req,res) => {
    ProductController.updateProduct(req.params.id,req.body).then(result => res.send(result))
})

// archive product (admin only))
router.delete('/:id/archive', auth.verify, auth.checkIsAdmin, (req,res) => {
    ProductController.archiveProduct(req.params.id).then(result => res.send(result))
})













module.exports = router