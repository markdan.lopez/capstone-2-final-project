const jwt = require('jsonwebtoken')
const secret = 'tiananmen'
const User = require('./models/user')


//create access token
module.exports.createAccessToken = (user) => {
    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    }

    return jwt.sign(data,secret,{})
}

// verify token
module.exports.verify = (req, res, next) => {
    let token = req.headers.authorization

    if(typeof token !== 'undefined'){
        token = token.slice(7, token.length)
        return jwt.verify(token, secret, (err,data)=>{
            return (err) ? res.send({auth: 'failed'}): next()
        })
    }else {
        return res.send({auth: 'failed'})
    }
}

// token decoder
module.exports.decode = (token) => {
    if(typeof token !== 'undefined'){
        token = token.slice(7, token.length)
        return jwt.verify(token, secret, (err, data)=> {
            return (err) ? null : jwt.decode(token, {complete: true}).payload
            // {complete: true} grabs both the request header and the payload
        })
    } else {
        return null;
    }
}

//For admin
module.exports.checkIsAdmin = (req,res,next) => {
    const user = this.decode(req.headers.authorization)
    if(user.isAdmin){
        next()
    } else {
        return res.send("For admin only. change your access token to admin's token")
    }
}

//For non-admin
module.exports.checkNonAdmin = (req,res,next) => {
    const user = this.decode(req.headers.authorization)
    if(user.isAdmin === false){
        next()
    } else {
        return res.send('Non-Admin Only')
    }
}