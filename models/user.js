const mongoose = require('mongoose')
const userSchema = new mongoose.Schema({


    email: {
        type: String,
        required: [true, 'Email is required.']
    },

    password: {
        type: String,
        required: [true, "Password is required."]
    },
    isAdmin: {
        type: Boolean,
        default: false // input true on the postman if you set it as admin
    },



    orders: [{
        purchasedOn: {
            type: Date,
            default: new Date()
        },
        productId: {
            type: String,
            required: [true, 'Product ID is required.']
        },
        totalAmount: {
            quantity: {
                type: Number,
                default: 1
            },
            totalPrice: {
                type: Number
            }
        }
    }]
})



module.exports = mongoose.model('User', userSchema)