const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')
const userRoutes = require('./routes/userRoutes')
const productRoutes = require('./routes/productRoutes')



const app = express()
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended: true}));



//database connection
mongoose.connect( /* YOUR MONGO DB URL */ {
	useNewUrlParser: true, //to use the new parser 
	useUnifiedTopology: true, // to use the new server discover and monitoring engine
	useFindAndModify: false
})

mongoose.connection.once('open', ()=> console.log('Now connected to the database'))

app.use("/products", productRoutes)
app.use("/users", userRoutes)


app.listen(process.env.PORT || 4000, ()=>{
    console.log(`API is now online at port ${process.env.PORT || 4000}`)
})
