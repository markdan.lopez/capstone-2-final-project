const Product = require('../models/product')
const bcrypt = require('bcrypt')
const auth = require('../auth')
const user = require('../models/user')


// get all the active products
module.exports.getActiveProducts = () => {
    return Product.find({isActive: true}).then((products, err) => {
        if(err){
            return err
        } else {
            return products
        }
    })
}

// get a product
module.exports.getProduct = (productId) => {
    return Product.findById(productId).then((product,err) => {
        if(err){
            return err
        } else {
            return product
        }
    })
}


// add/create a product (admin only)
module.exports.addProduct = (body) => {
    let product = new Product({
        name: body.name,
        description: body.description,
        price: body.price,
        isActive: body.isActive
    })

    return Product.findOne({name: product.name}).then(result => {
        if(result === null){
            return product.save().then((newProduct, err) => {
                if(err){
                    return err
                } else {
                    return newProduct
                }
            })
        } else {
            return (`Duplicate product found.`)
        }
    })
}

//update a product (admin only)
module.exports.updateProduct = (productId, params) => {
    return Product.findById(productId).then((product,err) => {
        if(err){
            console.log(err)
            return false
        }

        product.name = params.name,
        product.description = params.description,
        product.price = params.price

        return Product.findOne({name: product.name}).then(result => {
            if(result === null){
                return product.save().then((updatedProduct,saveErr) => {
                    if(saveErr){
                        console.log(saveErr);
                        return false
                    } else {
                        return updatedProduct;
                    }
                })
            } else {
                return (`Duplicate product found.`)
            }
        })
    })
}

//archive product (admin only)
module.exports.archiveProduct = (productId) => {
    return Product.findById(productId).then((product,err) => {
        if(err){
            return err
        }
        
        product.isActive = false;


        return product.save().then((archivedProduct,saveErr) => {
            if(saveErr){
                return saveErr
            } else {
                return archivedProduct
            }
        })
    })
}