const User = require('../models/user')
const bcrypt = require('bcrypt')
const auth = require('../auth')
const Product = require('../models/product')

module.exports.getUsers = () => {

    return User.find().then((users,err) => {
        
            return (err) ? err : users
        
        
    })
}

module.exports.register = (body) => {
    let user = new User({
        email: body.email,
        isAdmin: body.isAdmin,
        password: bcrypt.hashSync(body.password, 10)

    })
    return User.findOne({email: user.email}).then(result => {
        if(result === null){
            return user.save().then((user,err) => {
                return (err) ? false : true
            })
        } else {
            return (`Email is already associated to an existing user.`)
        }
    }) 
}

module.exports.login = (params) => {
    return User.findOne({email: params.email}).then(user => {
        if(user === null){
            return (`Email does not match any account.`)
        }

        const doPasswordsMatch = bcrypt.compareSync(params.password, user.password)

        if(doPasswordsMatch){
            return {accessToken: auth.createAccessToken(user.toObject())}
        } else {
            return (`Incorrect password.`)
        }
    })
}

module.exports.setAdmin = (userId) => {
    return User.findById(userId).then((user,err) => {
        if(err){
            console.log(err)
            return false
        }

        user.isAdmin = true

        return user.save().then((newAdmin, saveErr) => {
            if(saveErr){
                console.log(saveErr)
                return false
            } else {
                return newAdmin
            }
        })
    })
}

module.exports.addOrder = async (data) => {


    let productSaveStatus = await Product.findOne({_id: data.productId}).then((product,err) => {
        if(err){
            return err
        }

        
        let order = {
            productId: data.productId,
            totalAmount: {
                quantity: data.quantity,
                totalPrice: product.price * data.quantity
            }
        }

        return User.findById(data.userId).then(user => {
            user.orders.push(order);
            return user.save().then((user,err) => {
                if(err){
                    return false
                } else {
                    return true
                }
            })
        })
    })

    let userSaveStatus = await Product.findOne({_id: data.productId}).then(product => {
        product.purchases.push({userId: data.userId})
        return product.save().then((product,err) => {
            if(err){
                return false
            } else {
                return true
            }
        })
    })

    if(productSaveStatus && userSaveStatus){
        return true
    } else {
        return false
    }

    
}

module.exports.getOrders = (data) => {
    return User.findById(data.userId).then((user,err) => {
        if(err){
            return err;
        } else {
            if(user.orders.length === 0){
                return 'You have no orders.'
            } else {
                return user.orders
            }
        }
        
    })
}

module.exports.getAllOrders = () => {
    return User.find({"orders.0": {"$exists": true}, isAdmin: false},{orders: 1, email: 1}).then((users,err) => {
        if(err){
            return err
        } else {
            return users
        }
    })
}